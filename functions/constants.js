// This class contains system-defined global constants.

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
exports.functions = require('firebase-functions');

// The Firebase Admin SDK.
const fbAdmin = require('firebase-admin');
fbAdmin.initializeApp({
    credential: fbAdmin.credential.applicationDefault(),
});
exports.admin = fbAdmin;

// The Cloud Firestore
exports.db = fbAdmin.firestore();

/**
 * Types of the user of this application.
 */
exports.UserType = Object.freeze({
    TRAVELER_IN_SERVER: undefined, // Server-only value
    TRAVELER_IN_APP: -1, // App-friendly value, used as response only for the client.
    ADMIN: 0,
    AGENT: 1,
    ACCOUNTANT: 2,
});

/**
 * Status of the entity.
 */
exports.Status = Object.freeze({
    OK: 0,
    IN_PROGRESS: 1,
    FOR_REVIEW: 2,
    REVIEWED_OK: 3,
    REVIEWED_DECLINED: 4,
    // End state
    PAID: -1,
    DELETED: -2,
    CANCELLED: -3,
});

/**
 * The type of notification.
 */
exports.NotificationType = Object.freeze({
    TRAVEL_REVIEW: 'travel_review',
});