const constants = require('./constants.js');

/**
 * Validates the user whether it is authenticated and authorized.
 * @param {*} context The context of the user.
 * @param {*} allowedUserTypes The user types allowed.
 * @throws HttpsError when the user is not authenticated or authorized.
 */
exports.validateUser = function(context, allowedUserTypes) {
	// Checking that the user is authenticated.
    if (!context.auth) {
        constants.functions.logger.warn(`User is not authenticated.`, context.auth.uid);
		throw new constants.functions.https.HttpsError('permission-denied', 'User is not authenticated.');
    }
	// Checking that the user's type is authorized.
    const userType = context.auth.token.userType
    if (!(allowedUserTypes.includes(userType))) {
        constants.functions.logger.warn(`User is not authorized.`, context.auth.uid, userType);
		throw new constants.functions.https.HttpsError('permission-denied', 'User is not authorized.');
	}
}

/**
 * Return the variable if it is not null, else return the default value.
 * @param {*} nullableVar The nullable variable to be validated.
 * @param {*} defaultValue The default value.
 * @returns Whether the variable, or the default value.
 */
exports.ifNullThen = function(nullableVar, defaultValue) {
    if (typeof nullableVar !== 'undefined' && nullableVar !== null) {
        return nullableVar;
    } else {
        return defaultValue;
    } 
}

/**
 * Sends push notification message to a single device.
 * @param {*} registrationToken The device's registration token. This comes from the client FCM SDKs.
 * @param {*} title The title of the message.
 * @param {*} body The body of the message.
 * @returns The message's ID string.
 */
exports.sendPushNotif = async function(registrationToken, title, body) {
    if (typeof registrationToken === 'undefined' || registrationToken === null) {
        throw new Error(`Registration token cannot be empty or null.`);
    }
    var message = {
        token: registrationToken,
        notification: {
            title: title,
            body: body
        },
    };

    // Send a message to the device corresponding to the provided registration token.
    await constants.admin.messaging().send(message)
        .then((response) => {
            // Response is a message ID string.
            constants.functions.logger.info(`Successfully sent push notification:`, response);
            return response;
        })
        .catch((error) => {
            constants.functions.logger.error(`Error sending push notification:`, error);
        });
}