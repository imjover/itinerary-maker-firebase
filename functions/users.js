const constants = require('./constants.js');
const utils = require('./utils.js');

/**
 * When a user is newly registered, then process some update, e.g.
 * - add this user in the 'users' table.
 */
exports.handleNewlyRegisteredUser = constants.functions.auth.user().onCreate(async (user) => {
    const uid = user.uid;
    const username = utils.ifNullThen(user.email, uid);
    const name = utils.ifNullThen(user.displayName, username);

    constants.functions.logger.info("New user has registered", uid, username);

    // Initialize separate admin & DB here to skip credentials.
    // This will allow us to write new doc.
    const admin = require('firebase-admin');
    const db = admin.firestore();

    return db.collection('users').doc(uid)
        .set({
            username: username,
            name: name,
            photo_url: user.photoURL,
            user_type: constants.UserType.TRAVELER_IN_APP,
            is_approved: true
        })
        .then(() => {
            // Note to self: I just copied below code that was used on updating custom claims,
            // It seems this is also helpful in updating users record in DB.

			// TODO : Check if below feature has usage to your use-cases.
			// Update real-time database to notify client to force refresh.
			const metadataRef = constants.admin.database().ref("metadata/" + user.uid);
			// Set the refresh time to the current UTC timestamp.
			// This will be captured on the client to force a token refresh.
			return metadataRef.set({refreshTime: new Date().getTime()});
		})
		.catch(error => {
            constants.functions.logger.error("Failed to register new user", error);
		});
});

/**
 * For registration of non-traveler, call this function to request for approval.
 */
exports.requestRegistration = constants.functions.https.onCall((data, context) => {
    // The user trying to register should be a TRAVELER that wants to be part of the management team.
	utils.validateUser(context, [constants.UserType.TRAVELER_IN_SERVER])

    // Validate that the target user type to be registered is allowed.
    const userType = data.userType;
    const allowedUserTypes = [
        constants.UserType.AGENT, 
        constants.UserType.ACCOUNTANT
    ];
    if (!(allowedUserTypes.includes(userType))) {
		throw new constants.functions.https.HttpsError('failed-precondition', 'Target user type is not allowed.');
	}

    // Update the details of the user
    const uid = context.auth.uid;
	const customClaims = {
		userType: userType,
		isApproved: false
    };
    
    return constants.admin.auth().setCustomUserClaims(uid, customClaims)
        .then(async() => 
            constants.db.collection('users').doc(uid).update({
                user_type: userType,
                is_approved: false
            }
        ))
		.then(() => {
			return `Request for registration has been sent. Please contact your manager.`;
		})
		.catch(error => {
            constants.functions.logger.error("Failed to request registration of management user", error);
            throw new constants.functions.https.HttpsError('internal', 'An error occured.');
		});
});

/**
 * Approves the registration of a user.
 */
exports.approveRegistration = constants.functions.https.onCall(async(data, context) => {
    utils.validateUser(context, [constants.UserType.ADMIN])
    
	const uid = data.uid;
    const user = await constants.admin.auth().getUser(uid)
        .catch((error) => {
            console.error(error);
            throw new constants.functions.https.HttpsError('internal', 'This user is not available anymore.');
        });

    const name = utils.ifNullThen(user.displayName, utils.ifNullThen(user.email, uid));
	if (user.customClaims && user.customClaims['isApproved'] === true) {
        // If the user is already approved, then end.
        return `${name} is already registered.`;
    }
	const customClaims = {
		userType: user.customClaims['userType'],
		isApproved: true
	};
    return constants.admin.auth().setCustomUserClaims(uid, customClaims)
        .then(async() => 
            constants.db.collection('users').doc(uid).update({
                is_approved: true
            }
        ))
        .then(() => {
            return `User ${name} is now registered.`;
        })
		.catch(error => {
            constants.functions.logger.error("Failed to approve registration of management user", error);
            throw new constants.functions.https.HttpsError('internal', 'An error occured.');
		});
});

/**
 * Changes the user type of a user
 */
// exports.updateUserType = functions.https.onCall((data, context) => {
	
// 	// if (context.auth.token.userType !== 1) {
// 	// 	throw new functions.https.HttpsError('failed-precondition', 'Request not authorized. Invalid user type.');
// 	// }

// 	const uid = data.uid;
// 	const userType = data.userType;
// 	const customClaims = {
// 		userType: userType,
// 		isApproved: true
// 	};
// 	// Set custom user claims on this newly created user.
// 	return admin.auth().setCustomUserClaims(uid, customClaims)
// 		.then(() => {
// 			return { message: "Account type is changed." };
// 		})
// 		.catch(error => {
// 			console.log(error);
// 		});
// });

/**
 * Get all users.
 */
// exports.get = constants.functions.https.onCall((data, context) => {
//     utils.validateUser(context, [
//         constants.UserType.AGENT, 
//         constants.UserType.ACCOUNTANT, 
//         constants.UserType.ADMIN
//     ])

//     // Prepare user types
//     const userTypes = data.userTypes;
//     if (userTypes.includes(constants.UserType.TRAVELER_IN_APP)) {
//         // Make sure that type TRAVELER from app is changed to server's equivalent value
//         const index = userTypes.indexOf(constants.UserType.TRAVELER_IN_APP)
//         userTypes.splice(index, 1)
//         userTypes.push(constants.UserType.TRAVELER_IN_SERVER)
//     }
    
// 	// FIXME: Implement handling of fetching/returning more than 1000 users
// 	// See link for reference: https://firebase.google.com/docs/auth/admin/manage-users#list_all_users
// 	return admin.auth().listUsers(1000)
// 		.then((listUsersResult) => {
// 			var users = [];
// 			listUsersResult.users.forEach((userRecord) => {
//                 // Retrieve custom claims
//                 var userType;
//                 var isApproved;
//                 if (!userRecord.customClaims) {
//                     userType = constants.UserType.TRAVELER_IN_SERVER;
//                     isApproved = true;
//                 } else {
//                     userType = userRecord.customClaims['userType'];
//                     isApproved = userRecord.customClaims['isApproved'];
//                 }

//                 // Fetch only the needed userTypes
// 				if (!(userTypes.includes(userType))) {
// 					return; // return is considered as continue in JS's forEach
// 				}

// 				// console.log('user', userRecord.toJSON());
// 				const id = userRecord.uid;
// 				const username = utils.ifNullThen(userRecord.email, id);
// 				const name = utils.ifNullThen(userRecord.displayName, username);
//                 if (userType === constants.UserType.TRAVELER_IN_SERVER) {
//                     // Make sure that type TRAVELER is changed back to app-friendly value
//                     userType = constants.UserType.TRAVELER_IN_APP
//                 }
// 				const user = {
// 					id: id,
// 					username: username,
//                     name: name,
//                     photoUrl: userRecord.photoURL,
// 					userType: userType,
// 					isApproved: utils.ifNullThen(isApproved, false)
// 				};
// 				users.push(user);
// 			});
// 			if (listUsersResult.pageToken) {
// 				// List next batch of users.
// 				var nextBatchOfUsers = listAllUsers(listUsersResult.pageToken);
// 				users.concat(nextBatchOfUsers);
// 			}
// 			return { users: users }
// 		})
// 		.catch((error) => {
// 			console.error('Error listing users:', error);
//             throw new constants.functions.https.HttpsError('internal', 'An error occured.');
// 		});
// });

// FIXME: Implement handling of fetching/returning more than 1000 users
// See link for reference: https://firebase.google.com/docs/auth/admin/manage-users#list_all_users
// function listAllUsers(nextPageToken) {
// 	// List batch of users, 1000 at a time.
// 	return admin.auth().listUsers(1000, nextPageToken)
// 		.then(function(listUsersResult) {
// 			var users = [];
// 			listUsersResult.users.forEach(function(userRecord) {
// 				console.log('user', userRecord.toJSON());
// 				users.push(userRecord);
// 			});
// 			if (listUsersResult.pageToken) {
// 				// List next batch of users.
// 				var nextBatchOfUsers = listAllUsers(listUsersResult.pageToken);
// 				users.concat(nextBatchOfUsers);
// 			}
// 			return users;
// 		})
// 		.catch(function(error) {
// 			console.log('Error listing users:', error);
// 		});
// }