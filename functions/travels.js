const constants = require('./constants.js');
const utils = require('./utils.js');

/**
 * Submits the travel to be reviewed.
 */
exports.submitForReview = constants.functions.https.onCall(async (data, context) => {
    utils.validateUser(context, [constants.UserType.ADMIN, constants.UserType.AGENT]);

    const uid = context.auth.uid;
    const ownerId = data.ownerId;
    const travelId = data.travelId;
    const status = constants.Status.FOR_REVIEW;
    const reason = `Submitted for review`;
    
    // TODO : We can attempt to call updateReview first before fetching the travel's name
    var messagePushNotif;
    try {
        const travelRef = constants.db
            .collection('users').doc(ownerId)
            .collection('travels').doc(travelId);
        const travel = await travelRef.get();
        const travelName = travel.get('name');
        messagePushNotif = `Your travel to '${travelName}' is now ready for review.`;
    } catch (error) {
        constants.functions.logger.error("Failed to fetch recipient id", error);
        messagePushNotif = `Your travel is now ready for review.`;
    }

    try {
        await updateReview(
            uid,
            ownerId,
            travelId,
            status,
            reason,
            messagePushNotif,
            ownerId
        )
    } catch (error) {
        throw new constants.functions.https.HttpsError('internal', 'An error occured.');
    }
    return `You have sent a travel for review. The user will be notified.`;
});

/**
 * Submits the review on the travel.
 */
exports.submitReview = constants.functions.https.onCall(async (data, context) => {
    utils.validateUser(context, [constants.UserType.TRAVELER_IN_SERVER]);
    
    const uid = context.auth.uid;
    const ownerId = data.ownerId;
    const travelId = data.travelId;
    const status = data.isApprove ? constants.Status.REVIEWED_OK : constants.Status.REVIEWED_DECLINED;
    
    const name = context.auth.token.name || null;
    const statusText = status === constants.Status.REVIEWED_OK ? 'approved' : 'declined';
    const reason = utils.ifNullThen(data.reason, `Review ${statusText}`);
    const messagePushNotif = name !== null ?
        `A travel was ${statusText} by ${name}.` :
        `A travel was ${statusText}.`;
    
    var recipientId;
    try {
        const travelRef = constants.db
            .collection('users').doc(ownerId)
            .collection('travels').doc(travelId);
        const travel = await travelRef.get();
        recipientId = travel.get('fk_agent');
    } catch (error) {
        constants.functions.logger.error("Failed to fetch recipient id", error);
        throw new functions.https.HttpsError('internal', 'An error occured.');
    }

    try {
        await updateReview(
            uid,
            ownerId,
            travelId,
            status,
            reason,
            messagePushNotif,
            recipientId
        )
    } catch (error) {
        throw new functions.https.HttpsError('internal', 'An error occured.');
    }
    return `You have ${statusText} your travel. The travel agency will be notified.`;
});

/**
 * Updates a travel's review.
 * @param {*} uid The ID of the user who initiated this transaction.
 * @param {*} ownerId The ID of the owner of this transaction.
 * @param {*} travelId The ID of the travel to be updated.
 * @param {*} status The new status of the travel.
 * @param {*} reason The reason why this transaction was updated.
 * @param {*} messagePushNotif The notification's message to be sent to the recipient.
 * @param {*} recipientId The ID of the recipient of this transaction.
 */
async function updateReview(uid, ownerId, travelId, status, reason, messagePushNotif, recipientId) {
    // BEGIN TRANSACTION
    const travelRef = constants.db
        .collection('users').doc(ownerId)
        .collection('travels').doc(travelId);
    const notificationRef = constants.db
        .collection('users').doc(recipientId)
        .collection('notifications').doc();
    try {
        await constants.db.runTransaction(async (t) => {
            const timestamp = constants.admin.firestore.FieldValue.serverTimestamp();
            // 1. Update travel's status in DB
            t.update(travelRef, {
                status: status,
                status_reason: reason,
                payment_mode: null,
                last_updated: timestamp,
                fk_last_updated_by: uid,
            });

            // 2. Insert notification in DB
            t.set(notificationRef, {
                message: messagePushNotif,
                fk_submitted_by: uid,
                notification_type: constants.NotificationType.TRAVEL_REVIEW,
                transaction_id: travelId,
                timestamp: timestamp,
                is_seen: false
            });
        });
        constants.functions.logger.info(`Travel's review updated!`, ownerId, travelId);
    } catch (error) {
        constants.functions.logger.error("Failed to update travel review", error);
        throw error;
    }

    // 3. Send push notif
    try {
        const recipientRef = constants.db
            .collection('users').doc(recipientId);
        const recipient = await recipientRef.get();
        const deviceToken = recipient.get('device_token')

        if (typeof deviceToken !== 'undefined' && deviceToken !== null) {
            await utils.sendPushNotif(deviceToken, `Travel Review`, messagePushNotif)
        } else {
            constants.functions.logger.warn(`Push notification averted. Device token not found.`);
        }
    } catch (error) {
        constants.functions.logger.error("Failed to send push notification", error);
        // It's fine to fail on sending push notif, proceed on the transaction.
    }
}