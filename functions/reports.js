const constants = require('./constants.js');
const utils = require('./utils.js');

/**
 * Submits the travel to be reviewed.
 */
exports.getSalesReport = constants.functions.https.onCall(async (data, context) => {
    utils.validateUser(context, [constants.UserType.ADMIN, constants.UserType.AGENT, constants.UserType.ACCOUNTANT]);

    const startDate = new Date(data.startDate);
    const endDate = new Date(data.endDate);
    endDate.setHours(23)
    endDate.setMinutes(59)
    endDate.setSeconds(59)

    const map = {};
    try {
        const usersRef = constants.db
            .collection('users');
        // 1. Get IDs of all valid travelers
        const usersRefWithCondition = usersRef
            .where('user_type', '==', constants.UserType.TRAVELER_IN_APP);

        // constants.functions.logger.info(`Fetching users`);
        const users = await usersRefWithCondition.get();
        /* eslint-disable no-await-in-loop */
        for (const user of users.docs) {
            const travelsRef = constants.db
                .collection('users').doc(user.id)
                .collection('travels');

            // 2. Get IDs of all travels that are within the date range
            const travelsRefWithCondition = constants.db
                .collection('users').doc(user.id)
                .collection('travels')
                .where('status', '==', constants.Status.PAID)
                .where('payment_date', '>=', startDate)
                .where('payment_date', '<=', endDate);

            // constants.functions.logger.info(`Fetching travels of ${user.id}`);
            const travels = await travelsRefWithCondition.get();
            /* eslint-disable no-await-in-loop */
            for (const travel of travels.docs) {
                const itinerariesRef = constants.db
                    .collection('users').doc(user.id)
                    .collection('travels').doc(travel.id)
                    .collection('itineraries');
                
                // constants.functions.logger.info(`Fetching itineraries of ${travel.id}`);
                const itineraries = await itinerariesRef.get();
                /* eslint-disable no-await-in-loop */
                for (const itinerary of itineraries.docs) {
                    const destinationsRef = constants.db
                        .collection('users').doc(user.id)
                        .collection('travels').doc(travel.id)
                        .collection('itineraries').doc(itinerary.id)
                        .collection('destinations');
                    
                    // constants.functions.logger.info(`Fetching destinations ${itinerary.id}`);
                    const destinations = await destinationsRef.get();
                    /* eslint-disable no-await-in-loop */
                    for (const destination of destinations.docs) {
                        const expensesRef = constants.db
                            .collection('users').doc(user.id)
                            .collection('travels').doc(travel.id)
                            .collection('itineraries').doc(itinerary.id)
                            .collection('destinations').doc(destination.id)
                            .collection('expenses');
                        
                        // constants.functions.logger.info(`Fetching expenses of ${destination.id}`);
                        const expenses = await expensesRef.get();
                        /* eslint-disable no-await-in-loop */
                        for (const expense of expenses.docs) {
                            // 3. Group expenses by type
                            const expenseType = expense.get('expense_type');
                            const capital = expense.get('capital');
                            const price = expense.get('price');
                            // constants.functions.logger.info(`Expense ${expense.id}`, expenseType, capital, price);

                            const current = map[expenseType];
                            if (typeof current === 'undefined') {
                                map[expenseType] = [capital, price];
                            } else {
                                const newCapital = current[0] + capital;
                                const newPrice = current[1] + price;
                                map[expenseType] = [newCapital, newPrice];
                            }
                        }
                        /* eslint-disable no-await-in-loop */
                    }
                    /* eslint-disable no-await-in-loop */
                }
                /* eslint-disable no-await-in-loop */
            }
            /* eslint-disable no-await-in-loop */
        }
        /* eslint-disable no-await-in-loop */
        constants.functions.logger.info(`Report generated for dates between ${startDate} and ${endDate}`, map);
    } catch (error) {
        constants.functions.logger.error("Failed to retrieve report data", error);
        throw error;
    }
    return map;
});